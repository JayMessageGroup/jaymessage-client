# JayMessage-client
Open-source client for the JPay prison email system, written in Python.

_This project is not associated with JPay LLC. or Securus Technologies LLC._

This program uses JPay's API to retrieve messages, show VideoGrams, check stamps, and more.

## Features

Currently, JayMessage can read and download messages, as well as view VideoGrams. We eventually intend to support the same features the official JPay app does.

| Features               | JayMessage | JPay App |
|------------------------|------------|----------|
| Viewing mail           | ✔️          | ✔️        |
| Sending mail           | ❌(planned) | ✔️        |
| Downloading mail       | ✔️          | ❌        |
| Viewing VideoGrams     | ✔️(partial) | ✔️        |
| Downloading VideoGrams | ✔️(partial, can use browser to download) | ❌        |

Once the desktop Python version is stable and reaches feature-parity with the official JPay app, we'd like to release for mobile platforms, such as Android.

## Versions

### 0.0.1 - Initial release

### 0.1.0 - Released 2021-08-02; JayMessage can now save all messages to a text file. Also includes various bugfixes/enhances.

### 0.2.0 - Released 2021-08-09; Added partial VideoGram support 🎉 and slightly better error handling.
#### 0.2.1 - Released 2021-08-09; added getpass(), minor fixes
